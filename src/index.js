import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.js';

import { Provider } from 'react-redux' 
import { configureStore } from './store' // 6. tuodaan store, mikä tehtiin Store.js tiedostossa

import {persistStore} from 'redux-persist'
import {PersistGate} from 'redux-persist/lib/integration/react' // 7. nämä kirjastot on staten säilymiseen tarvittavia


const store = configureStore();
const persistor = persistStore(store);

ReactDOM.render(
    <Provider store={store}> 
    <PersistGate  persistor={persistor} loading={<div>loading...</div>} >
        <App />
        </PersistGate>
    </Provider>,

    document.getElementById('root')
);

//8. wrapätään reactin main <app /> provideriin. providerin storena on store.js tiedostosta tullut store

//8.1 <persistGate> wräpätään providerin sisälle. Se ottaa argumentiksi persistorin (persisStore(haluttu store)) ja 
// loadin (mitä halutaan näyttää sivun latauksen aikana)

// => todos/actions.js