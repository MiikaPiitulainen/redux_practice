import React from 'react'

const TodoListItem = ({ todo, onRemovePressed, onIsCompletedPressed }) => (
    <div className="todo-item-containter">
        <h3>{todo.text}</h3>
        <h3>{todo.date}</h3>
        <div className="buttons-container">
            {todo.isCompleted
                ? null
                : <button
                    onClick={() => onIsCompletedPressed(todo.text)}
                    className="completed-button">complete</button>
            }
            <button
                onClick={() => onRemovePressed(todo.text)}  // 19. ja täällä propsit otetaan vastaan jne. kun removea painetaan, niin tehdään onremovepressed action
                className="remove-button">remove</button>      
        </div>

    </div>
)

export default TodoListItem;