import React, { useState } from "react";
import { connect } from 'react-redux' // 13. 
import { createTodo } from './actions'





const NewTodoForm = ({ todos, onCreatePressed }) => { // 14.2/ 15.1 todosin voi sitten laittaa tänne
    const [inputValue, setInputValue] = useState('');
    const [dateValue, setDateValue] =useState('')
    return (
        <div className="new-todo-form">
            <input
                className="new-todo-input"
                type="text"
                placeholder="Type new todo"
                value={inputValue}
                onChange={e => setInputValue(e.target.value)} />
            <input
                className="new-todo-date-input"
                type="date"
                value={dateValue}
                onChange={e => setDateValue(e.target.value)} />
            <button
                onClick={() => {                                                     // 16. sitten tehdään buttonille toiminnallisuus!
                    const isDuplicateText =                                         // ensin katsotaan, että vastaako tekstikentässä kirjoitettu teksti mitään statessa olevaa tekstiä
                        todos.some(todo => todo.text === inputValue)                 // jos vastaa, niin mitään ei tapahdu. jos ei, niin tehdään onCreatePressed action
                    if (!isDuplicateText & inputValue != '' & dateValue!= '') {       // (eli tehdään dispatch createTodo(text)). ==> todolist.js
                        onCreatePressed(inputValue, dateValue);
                            setInputValue('');
                            setDateValue('')
                    }

                }}
                className="new-todo-button">
                Create Todo</button>
        </div>
    )
}

const mapStateToProps = state => ({ // 14.1 mapstatetopropsissa määritetään, mihin funktiossa määritettyyn (tässä se on state) osaan/osiin tämän komponentin tulisi päästä
    todos: state.todos              // (tässä halutaan päästä kaikkiin todo osiin)
})
const mapDispatchToProps = dispatch => ({
    onCreatePressed: (text, date) => dispatch(createTodo(text, date)) // 15. tämä toimii samalla tavalla, mutta ottaa argumentiksi dispatchin. Tämän funktion avulla voidaan tehdä actioneita
})


export default connect(mapStateToProps, mapDispatchToProps)(NewTodoForm);
// 14. kun yhdistetään reactin storeen, niin exportataan connect. Ensimmäiseen sulkeeseen tulee 
//
// toiseen sulkeeseen tulee tämän komponentin nimi (newtodoform)