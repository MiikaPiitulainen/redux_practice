export const CREATE_TODO = 'CREATE_TODO';

export const createTodo= (text, date) =>({
    type: CREATE_TODO,
    payload: {
         text,
         date,
        isCompleted: false },
})

export const REMOVE_TODO ='REMOVE_TODO';

export const removeTodo = text =>({
    type: REMOVE_TODO,
    payload: { text },
})


export const IS_COMPLETED ='IS_COMPLETED';

export const isCompleted = text =>({
    type: IS_COMPLETED,
    payload: { text },
})

// 9. huom tässä tutoriaalissa actionit on funktioita. niihin tulee "text" argumentti. actioni sitten palauttaa objektin, missä on type
// ja toinen objekti, missä on kaikki textin ominaisuudet. nämä on siis "action creatoreita", mitkä helpottaa ohjelmointia reactin kanssa

// => reducers.js
