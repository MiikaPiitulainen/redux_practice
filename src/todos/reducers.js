import { CREATE_TODO, REMOVE_TODO, IS_COMPLETED } from './actions'

// 10. importataan kaikki ./actions.js actionit (string?)


export const todos = (state = [], action) => {
    const { type, payload } = action; // 11. otetaan actionin tyyppi ja payload omiksi muuttujiksi
    switch (type) {                     // 12. tyypin mukaan vaihdetaan reduceria. perus switch state
        case CREATE_TODO: {
            const { text, date, isCompleted } = payload; // 13. otetaan payloadin teksti omaksi muuttujaksi.
            const newTodo = {           // 14. määritetään uusi todo muistilapun ominaisuudet.
                text,
                date,
                isCompleted
            }
            return state.concat(newTodo) // 15. palautetaan UUSI state, missä on otettu edellisen staten array ja lisätty siiden uusi newtodo
                                            // => store.js
        }
        case REMOVE_TODO: {
            const { text } = payload;
            return state.filter(todo => todo.text != text)
        }
        case IS_COMPLETED: {
            const {text} = payload;
            const noteToChange = state.find(n => n.text === text)
            console.log(noteToChange)
            const changedNote ={
                ...noteToChange,
                isCompleted: !noteToChange.isCompleted
            }
            return state.map (complete =>
                complete.text !== text ? complete : changedNote)
        }
        default:
            return state; 
    }
}