import React from "react";
import NewTodoForm from './NewTodoForm'
import TodoListItem from './TodoListItem'

import { connect } from 'react-redux'
import { isCompleted, removeTodo } from './actions'


const TodoList = ({ todos = [], onRemovePressed, onIsCompletedPressed }) => (
    <div className="list-Wrapper">
        <NewTodoForm />
        {todos.map(todo =>
            <TodoListItem
                todo={todo}
                onRemovePressed={onRemovePressed}
                onIsCompletedPressed={onIsCompletedPressed} /> // 18. tehdään samat hommat kuin newtodoformissa, eli mapstate ja dispatch.
        )}                                                      
    </div>                                                      // laitetaan sitten kaikille todolistitemille propsit, missä on kaikki halutut actionit ja nykyinen state
)                                                               // => todolistitem.js

const mapStateToProps = state => ({
    todos: state.todos
})
const mapDispatchToProps = dispatch => ({
    onRemovePressed: text => dispatch(removeTodo(text)),
    onIsCompletedPressed: text => dispatch(isCompleted(text))
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);