import { createStore, combineReducers } from 'redux' //1. 
import { todos } from './todos/reducers'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import autoMergeLeve2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

const reducers = {
    todos,
}
//12. lisätään reduceri, mikä tehtiin reducers.js tiedostossa
// => newtodoform.js

//2. tähän listataan kaikki reducerit, mitä halutaan käyttää ohjelmassa

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLeve2,
}


const rootReducer = combineReducers(reducers);

// 3. tässä yhdistetään kaikki reducerit yhdeksi "pää" reduceriksi

const persistedReducer = persistReducer(persistConfig, rootReducer)

// 4. PresistedReducerin avulla, state ei resetoidu, kun ohjelma refreshaa

export const configureStore = () =>
    createStore(
        persistedReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ &&
        window.__REDUX_DEVTOOLS_EXTENSION__()
    )

// 5. täällä tehdään storen config, minne laitetaan pääreduceri. kaksi viimeistä riviä on redux devtoolsille.

// => Index.js